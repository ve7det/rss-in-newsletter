Assembles the News Section for VECTOR's Newsletter using the blog entries
===================

For VECTOR's newsletter, we assemble a single message to update members when we remind them about our monthly members session.  This project allows the person assembling the newsletter to create modestly detailed articles on the website and collect them in a summary list for the newsletter.  

The person distributing the newsletter can then use the resulting summary when preparing the newsletter email body.  

## Table of Contents

* [Usage](#usage) 
* [Installation](#installation) 
* [Uses](#uses) 
* [Needs](#needs) 
* [Contributing](#contributing) 
* [Credits](#credits) 
* [License](#license) 
* [Donations](#donations)


## Usage

This project involves using RSS feeds from the VECTOR website to capture the headline, overview, and referenceable link and creater a file for the volunteer distributing the newsletter.  

1. Write entries to the VECTOR website as you learn of them:  blog (tag "news", or category "news"), workshops items, program items.  

2. Run make 

```shell 
$ make 
```


## Installation 

1. Download the repository to where you intend to work  
2. Extract the project into the folder  


## Needs

Needs entries on the VECTOR website.  

Section | Purpose
---|---
`content/post` | items for the news section
`content/learn` | items for the workshops section
`content/program` | items for the on-air activities section


## Requirements

 * Python
 * Feedparser
 * Newspaper
 * Automake

<!--
#### Debian
```bash
sudo apt install python3-feedparser
```

#### Fedora
```bash
sudo dnf install pandoc texlive-collection-context
```

#### Arch
```bash
sudo pacman -S pandoc texlive-core
```
-->
## Troubleshooting


## Contributing

Please do.  

## Credits

* Jesse VE7DET

## License

Creative Commons Share and Share Alike, latest.

## Donations

We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)

