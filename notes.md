### Actions

- DONE write self-documenting makefile

### Refernce files

~/2018/outputs/organizations/2020-orgfinder

~/Outputs/scripts

### bash

git push --set-upstream git@gitlab.com:ve7det/$(git rev-parse --show-toplevel | xargs basename).git $(git rev-parse --abbrev-ref HEAD)

### fish

git push --set-upstream git@gitlab.com:ve7det/(git rev-parse --show-toplevel | xargs basename).git (git rev-parse --abbrev-ref HEAD)

