# For working on your local system.  

NEWS_FILE=news
OUTPUT_FOLDER=~/vector/git/Newsletter/blocks
OUTPUT_FILE=030-News

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

all: news publish ## Retrieve news and publish to Newsletter system

.PHONY: news
news: ## Retrieve news RSS's
	python3 pull.py

.PHONY: publish
publish: ## Copy to local git for VECTOR newsletter work
	cp $(NEWS_FILE).md $(OUTPUT_FOLDER)/$(OUTPUT_FILE).md -vu

#.PHONY: test
test:  ## Test the news output
	python3 pull.py test

.PHONY: clean
clean: ## Remove news collection
	rm -Rv $(NEWS_FILE).md
