#!/usr/bin/env python

# Git project:  rss-in-newsletter

import feedparser
from bs4 import BeautifulSoup
from time import mktime
from datetime import datetime, timedelta
import dateutil.parser as dp
import dateutil.tz as dtz
import hashlib, sys, getopt
import contextlib
from datetime import datetime
from time import mktime

# remind
print('Script for pulling news from RSS')

# limit age of articles to include, in days

agewindow = 31
outputfile = 'news.md'
print('     Initialized')

# collect feeds

print('Collecting feeds...')
newsfeed = feedparser.parse('https://vectorradio.ca/tags/news/rss.xml')
eventsfeed = feedparser.parse('https://vectoradio.ca/program/rss.xml')
workshopsfeed = feedparser.parse('https://vectorradio.ca/learn/rss.xml')
print('...[OK]')

# show how many total entries in feeds

#print('News entries: ', newsfeed.entries)
#print('Events entries: ', eventsfeed.entries)
#print('Workshops entries: ', workshopsfeed.entries)

# display times

now = datetime.now(dtz.tzlocal())
amonthago = now - timedelta(days=agewindow)

print('Current Time: ', now)
print('Start Collection from: ', amonthago)


# count entries to set limit on entries

def get_entries(feed):
    NEW_POST = u"""New post, author {author}, title {title} {content}"""
    for entry in feed.entries:
        if "http" in entry.id:
            nid = hashlib.md5(str(entry.id).encode('utf-8'))
            entry.id = nid.hexdigest()
        entry_content = entry.content[0].value
        soup = BeautifulSoup(entry_content, 'html.parser')
        chunks = list(chunks)
        published = dateutil.parser.parse(entry.published)
        for agewindow, chunk in enumerate(chunks):
            if agewindow == 0:
                chunk = NEW_POST.format(
                        title=entry.title,
                        content=chunk)
                print(chunk[agewindow])
            yield dict(
                content=chunk,
                id="%s_%d" % (entri.id, agewindow),
                title=entry.title,
                published=published - timedelta(0,agewindow),
            )
            remaining = chunk
#    return count


print('\n\nEntries (news):', get_entries(newsfeed))
#print('\n\nEntry Count (news):', get_entries(count))
# show article dates
anothercounter = 0
print('\nArticle Dates\n-------------')
for idnumber in range(len(newsfeed.entries)):
    anothercounter += 1 
    headline = newsfeed.entries[idnumber].title
    message = newsfeed.entries[idnumber].message
    entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
    showdate = newsfeed.entries[idnumber].published

# check age of article
    amonthagoforcompare = amonthago.replace(tzinfo=None)
#    itemdate = newsfeed.entries[idnumber].published
    dateforcompare = entrydate.replace(tzinfo=None)
#    print('Entry dated', entrydate, 'or', dateforcompare, 'without TZ')
    datedifference = amonthagoforcompare - dateforcompare
#    print(datedifference)
    if dateforcompare >= amonthagoforcompare:
        print(anothercounter, 'Item less than 1 month old: ', datedifference, '\n')
        print('\t', headline,'\n\t', entrydate, ' or ', dateforcompare, ' without TZ')
# convert time structure to datetime element - https://stackoverflow.com/questions/1697815/how-do-you-convert-a-time-struct-time-object-into-a-datetime-object
#    dt = datetime.fromtimestamp(mktime(struct))

#    published = dp.parse(entry.published)
#    print('1. Current: \n', newsfeed.entries[idnumber].published)
#    print('2. Current feedparser parsed: \n', newsfeed.entries[idnumber].published_parsed)
#    print('3. mktime ', mktime(newsfeed.entries[idnumber].published_parsed))
#    entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published))
        print('\t Current datetime parsed: \n\t', dp.parse(showdate), '\n')
#    print('Difference to amonthago ', datedifference)
#    print('Current: \n', newsfeed.entries[idnumber])
#    relativestate = dateforcompare - amonthagoforcompare
#    print('relative state ', relativestate)
    else:
        print(anothercounter, 'Item is too old to include')
        print('\n\t', headline, '\n\t', showdate, '\n')


# write summaries

print('## News \n\n-------------')
for idnumber in range(len(newsfeed.entries)):
    headline = newsfeed.entries[idnumber].title
    message = newsfeed.entries[idnumber].message
    entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
    showdate = newsfeed.entries[idnumber].published
    weblink = newsfeed.entries[idnumber].link

# check age of article
    amonthagoforcompare = amonthago.replace(tzinfo=None)
    entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
#    itemdate = newsfeed.entries[idnumber].published
    dateforcompare = entrydate.replace(tzinfo=None)
#    print('Entry dated', entrydate, 'or', dateforcompare, 'without TZ')
    datedifference = amonthagoforcompare - dateforcompare
#    print(datedifference)
    if dateforcompare >= amonthagoforcompare:
#        print('Item less than 1 month old')
        print('\n###', headline,'\n')
        print(message,'\n')
        print('[Full details...](', weblink,')\n\n--')

#    print('\n\n---')

# output news to file
if len(sys.argv) == 2 and sys.argv[1] == "test":
    print('Test complete.')
elif len(sys.argv) == 1:
# check age of article
    sys.stdout = open(outputfile,"w")
    print('## 2. Community News - What\'s Coming\n\n---\n\n')
    for idnumber in range (len(newsfeed.entries)):
        headline = newsfeed.entries[idnumber].title
        weblink = newsfeed.entries[idnumber].link
        message = newsfeed.entries[idnumber].message
        entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
        showdate = newsfeed.entries[idnumber].published
# make this testing a function
        amonthagoforcompare = amonthago.replace(tzinfo=None)
        entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
        dateforcompare = entrydate.replace(tzinfo=None)
        datedifference = amonthagoforcompare - dateforcompare
        if dateforcompare >= amonthagoforcompare:
# limit of the testing function
            print('- [', headline, '](', weblink, ')')


    print('\n\n-- --\n')
    for idnumber in range(len(newsfeed.entries)):
        headline = newsfeed.entries[idnumber].title
        message = newsfeed.entries[idnumber].message
        entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
        showdate = newsfeed.entries[idnumber].published
        weblink = newsfeed.entries[idnumber].link
# make this testing a function
        amonthagoforcompare = amonthago.replace(tzinfo=None)
        entrydate = datetime.fromtimestamp(mktime(newsfeed.entries[idnumber].published_parsed))
        dateforcompare = entrydate.replace(tzinfo=None)
        datedifference = amonthagoforcompare - dateforcompare
        if dateforcompare >= amonthagoforcompare:
# limit of the testing function
            print('\n###', headline,'\n')
            print(message,'\n')
            print('[Full details...](', weblink,')\n\n--')

    sys.stdout.close()

#print('## Events \n\n----------')
#print(eventsfeed.entries)
#for idnumber in range(len(newsfeed.entries)):
#    print('\n### ', eventsfeed.entries[idnumber].title)
#    print(eventsfeed.entries[idnumber].message)
#    print(eventsfeed.entries[idnumber].link)


#print('## Workshops \n\n----------')
#print(workshops.entries[1])
#for idnumber in range(len(newsfeed.entries)):
#    print('\n### ', workshopsfeed.entries[idnumber].title)
#    print(workshopsfeed.entries[idnumber].message)
#    describeit = BeautifulSoup(workshopsfeed.entries[idnumber].description, features="lxml")
#    print('Description: \n', describeit.get_text())
#    print('[', idnumber,']:', workshopsfeed.entries[idnumber].link)



